### Nike Interview Backend Code Challenge [NodeJS]

_The provided code document should contain more details._

Start server (Port 8081): `npm install -> npm start`

APIs:


1. Clone [this project](https://gitlab.com/hiring_nike_china/nike-shoes-backend-nodejs) and start this nodejs server, following the instructions on this project. The project should run at port 9090
2. Get original price (randomly fetched) for a supplied shoe id:
```
URL (GET) - http://localhost:8081/api/shoe-price/1

Response:
{
    "shoePrice": 147
}
```


Here are my working priority:

1. thinking, assuming

To be honest, when I get the document, I'm curious about why we need fetch data from  randomprice api, though it takes me some time to find it. ( in fact, we should clone three repository, this are not mentioned in the PDF) I guess these projects are more like a scrapy tool or price reminder, users can config their whish list when the shoes price are right. In these case, it's better if configuration be supported.

2. development

    a. as it related to dollors, I think the price should be accurrate. so I add parameter varification for now. Authentification will also needed.

    b. I make the the  flexible data, or configurable data into a config file for future on backend, these should applied to frontend too. The config file should be unifiled management, the other application can get these files through api. And in the future, we can develop a configuration system if needed. 

    c. Selfcheckiing, checking the code, and run the case by myself.

3. unit test

    In my personal experience , I like develop first ,then unit test follow. considering the self coding behavior and reality, I have no idea which one should come first. Speaking is always different to doing. 
    But there is one thing I confirmed. Unit test definately needed. This time , when I write some test cases, I found some logical issue in my code, it helps me to do self checking, enhanced the code, and it's also a documentation for others.

4. to do list, further enhancement

    a. global language should be supported as nike serves global users.

    b. I guess the cron reminder or notification is needed for user to get the latest information.

    c. authentification for api is needed.

    d. data security if the data comes from our database.

    e. config file should be more flexible, discount type should be more like price increase , decrease.

    f. performance enhancement, the bulk data handle is needed since I see the there is an api loop in frontend.

    g. in my opinion, I use config file here, so I need the config file  to be used by api.

    h. syntax, browser compatibility.
