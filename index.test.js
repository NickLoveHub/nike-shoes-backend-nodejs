const query = require('./index');
var should = require('should');
var assert = require('assert');
var request = require('supertest');

describe('Routing', function() {
  var url = 'http://localhost:8081';
  before(function(done) {
   done();
  });

  describe('/api/shoe-price/', function() {
    it('shoe-price api should return an object', function(done){
      request(url)
        .get('/api/shoe-price/1')
        .expect(200) //Status code
        .end(function(err,res) {
          if (err) {
            throw err;
          }
          console.log("success res==>",res.body);
          res.body.shoePrice.should.type('number')       
          done();
        });
    });

    describe('/api/shoe-discount-price/', function() {
        it('discount price api should return an object', function(done){
          request(url)
            .get('/api/shoe-price/1')
            .expect(200) //Status code
            .end(function(err,res) {
              if (err) {
                throw err;
              }
              console.log("success res==>",res.body);
              res.body.shoePrice.should.type('number')       
              done();
            });
        });

        it('discount price api invalid parameter validation', function(done){
            request(url)
              .get('/api/shoe-discount-price/test')
              .expect(500) //Status code
              .end(function(err,res) {
                console.log("success res==>",res.body);
                res.body.status.should.eq('failed')       
                done();
              });
          });        
    });
  });
});