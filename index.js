const express = require('express');
const fetch = require('node-fetch');
const cors = require('cors');

const app = express();
const port = 8081;

app.use(cors());

app.get('/api/shoe-price/:id', (req, res) => {
    fetch(`http://localhost:9090/fetch-random-price/${req.params.id}`)
        .then((response) => response.json())
        .then((response) => res.send(response))
        .catch((e) => {
            console.error('request error', e);
            return e.meta;
        });
});

app.get('/api/shoe-discount-price/:id', (req, res) => {
    // make the dicount data and max-min range data to be the bargain configuration, since the these two parts should be flexible    
    const isIntNum = (str) => {
        var n = Math.floor(Number(str));
        return n !== Infinity && String(n) === str && n >= 0;
    }
    if (!isIntNum(req.params.id)) { res.send({"status":"failed", "message":"invalid parameter"}) }

    const bargainConfig = require("./bargain.json");
    const applyChange = (bargainConfig, originalPrice) => {
        let discountConf = bargainConfig.shoes.discount || '';
        if (discountConf && discountConf.applied) {
            if (discountConf.appliedShoes && discountConf.appliedShoes.all) {
                return Math.round(originalPrice * 100 * discountConf.count) / 100;
            }
            //TODO applied to specific shoes
        }
        // TODO: there should be other type of price changes
    }
    fetch(`http://localhost:9090/fetch-random-price/${req.params.id}`)
        .then((response) => response.json())
        .then((response) => res.send({ 'originalPrice': response.shoePrice, 'discountPrice': applyChange(bargainConfig, response.shoePrice) }))
        .catch((e) => {
            console.error('request error', e);
            return res.status(500);
        });
})

// user config api for frontend, not testing now
app.get('/api/user-config/', (req, res) => {
    const bargainConfig = require("./bargain.json");
    res.send(JSON.stringify(bargainConfig.user ));
});


app.listen(port, () => {
    console.log(`Started backend server at ${port}`);
});
